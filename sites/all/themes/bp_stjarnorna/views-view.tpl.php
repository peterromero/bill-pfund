<?php
	if ($view->name=='catalog_categories') {
	}
	echo("<div class=\"$classes\">");
		echo(render($title_prefix));
		if ($title) {
			echo($title);
		}
		echo(render($title_suffix));
		if ($header) {
			echo("<div class=\"view-header\">$header</div>");
		}
		if ($exposed) {
			echo("<div class=\"view-filters\">$exposed</div>");
		}
		if ($attachment_before) {
			echo("<div class=\"attachment attachment-before\">$attachment_before</div>");
		}
		if ($rows) {
			echo("<div class=\"view-content\">$rows</div>");
		} elseif ($empty) {
			echo("<div class=\"view-empty\">$empty</div>");
		}
		if ($pager) {
			echo($pager);
		}
		if ($attachment_after) {
			echo("<div class=\"attachment attachment-after\">$attachment_after</div>");
		}
		if ($more) {
			echo($more);
		}
		if ($footer) {
			echo("<div class=\"view-footer\">$footer</div>");
		}
		if ($feed_icon) {
			echo("<div class=\"feed-icon\">$feed_icon</div>");
		}
	echo("</div>");
?>