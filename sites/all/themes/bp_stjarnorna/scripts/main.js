(function($) {
	$(function() {
		$('input[type=text], input[type=password]').focus(function() {
			$(this).select();
		});
		$('.messages').each(function() {
			if ($(this)[0].outerHTML.indexOf('update immediately') != -1) {
				$(this).hide();
			}
		});
	});
} (jQuery));