<?php
	$clearfix = true;
	if (property_exists($element['#object'], 'view')) {
		switch($element['#object']->view->name) { //Omit clearfix based on view.
			case 'links': {
				$clearfix = false;
				break;
			}
			default: {
				$clearfix = true;
				break;
			}
		}
	}
	$clearfix = ($clearfix)?"clearfix":"";
	echo("<div class=\"$classes $clearfix\"$attributes>");
		if (!$label_hidden) {
			echo("<div class=\"field-label\"$title_attributes>$label:&nbsp;</div>");
		}
		echo("<div class=\"field-items\"$content_attributes>");
			foreach ($items as $delta => $item) {
				switch ($element['#field_name']) {
					case "field_website": {
						$content = "<div class=\"field-item " . (($delta % 2) ? 'odd' : 'even') . "\"$item_attributes[$delta]><a href=\"" . render($item) . "\" target=\"_blank\">" . render($item) . "</a></div>";
						break;
					}
					case "field_email": {
						$content = "<div class=\"field-item " . (($delta % 2) ? 'odd' : 'even') . "\"$item_attributes[$delta]><a href=\"mailto:" . render($item) . "\">" . render($item) . "</a></div>";
						break;
					}
					default: {
						$content = "<div class=\"field-item " . (($delta % 2) ? 'odd' : 'even') . "\"$item_attributes[$delta]>" . render($item) . "</div>";
						break;
					}
				}
				echo($content);
			}
		echo("</div>");
	echo("</div>");
?>