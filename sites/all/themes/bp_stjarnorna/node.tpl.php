<?php
	echo("<div id=\"node-{$node->nid}\" class=\"$classes clearfix\" $attributes>");
	echo($user_picture);
	echo(render($title_prefix));
	if (!$page) {
		switch($node->type) {
			case 'faq': {
				echo("<h2>$title</h2>".render($title_suffix));
				if ($display_submitted) {
					echo("<div class=\"submitted\">$submitted</div>");
				}
				echo("<div class=\"content\" $content_attributes>");
					// We hide the comments and links now so that we can render them later.
					hide($content['comments']);
					hide($content['links']);
					echo(render($content));
				echo("</div>");
				echo(render($content['links']));
				echo(render($content['comments']));
				break;
			}
			case 'testimonial': {
				if ($display_submitted) {
					echo("<div class=\"submitted\">$submitted</div>");
				}
				echo("<div class=\"content\" $content_attributes>");
					// We hide the comments and links now so that we can render them later.
					hide($content['comments']);
					hide($content['links']);
					echo(render($content));
				echo("</div>");
				echo(render($content['links']));
				echo(render($content['comments']));
				break;
			}
			case 'colleague': {
				$websites = field_get_items("node", $node, "field_website");
				$website = field_view_value("node", $node, "field_website", $websites[0]);
				echo("<h2><a href=\"{$website['#markup']}\" target=\"_blank\">$title</a></h2>".render($title_suffix));
				if ($display_submitted) {
					echo("<div class=\"submitted\">$submitted</div>");
				}
				echo("<div class=\"content\" $content_attributes>");
					// We hide the comments and links now so that we can render them later.
					hide($content['comments']);
					hide($content['links']);
					echo(render($content));
				echo("</div>");
				echo(render($content['links']));
				echo(render($content['comments']));
				break;
			}
			case 'slider_link': {
				// $urls = field_get_items('node', $node, 'field_url');
				// $url = field_view_value('node', $node, 'field_url', $urls[0]);
				// $images = field_get_items('node', $node, 'field_image');
				// $image = field_view_value('node', $node, 'field_image', $images[0]);
				// $image_code = theme($image['#theme'], $image);
				// echo("<p>".print_r($image,true)."</p>");
				// echo("<a href=\"{$url['#markup']}\">$image_code</a>");
				$view = views_embed_view('slider_links');
				// $view = str_replace("class=\"views-row", "class=\"views-row slide", $view);
				echo($view);
				break;
			}
			default: {
				echo("<h2><a $title_attributes href=\"$node_url\">$title</a></h2>".render($title_suffix));
				if ($display_submitted) {
					echo("<div class=\"submitted\">$submitted</div>");
				}
				echo("<div class=\"content\" $content_attributes>");
					// We hide the comments and links now so that we can render them later.
					hide($content['comments']);
					hide($content['links']);
					echo(render($content));
				echo("</div>");
				echo(render($content['links']));
				echo(render($content['comments']));
				break;
			}
		}
	} else {
		switch($node->type) {
			case 'instrument': {
				// echo("<h2><a $title_attributes href=\"$node_url\">$title</a></h2>".render($title_suffix));
				
				// As of right now, this section adds code to change the Add to Cart button into a Purchase button that simply displays an alert message and doesn't allow the product to be added to the cart if the stock is zero. This happens regardless of whether stock is being tracked on the product or products in question. The stock_is_being_tracked value doesn't really work with products with multiple options because it is being pulled for the first row matching the $node->nid, which is random and may or may not be the base product SKU (which often has stock tracking turned off, even if SKUs with attribute combinations have it turned on).

				//Pull from the database all SKUs as related to possible combinations of attributes for the current node.
				//Join those results with the actual in-stock quantity for those product SKUs.
				$combinations = db_query("
					select
						`drupal_uc_product_adjustments`.`combination` as serialized,
						`drupal_uc_product_stock`.`stock` as qty,
						`drupal_uc_product_stock`.`active` as stock_is_being_tracked
					from
						`drupal_uc_product_adjustments`,
						`drupal_uc_product_stock`
					where
						`drupal_uc_product_adjustments`.`nid` = {$node->nid}
					and
						`drupal_uc_product_stock`.`sku` = `drupal_uc_product_adjustments`.`model`
				");
				foreach ($combinations as $combination) {
					//Unserialize and reserialize the attribute arrays as JSON. Plug them into hidden fields
					//with the SKU as the ID and the value as the JSON-serialized array.
					$attribute_array = unserialize($combination->serialized);
					ksort($attribute_array);
					$json = json_encode($attribute_array);
					echo("<input class=\"attribute_combination\" type=\"hidden\" value='$json' rel=\"{$combination->qty}\">");
				}
				$base_products = db_query("
					select
						`drupal_uc_product_stock`.`stock` as qty,
						`drupal_uc_product_stock`.`active` as stock_is_being_tracked
					from
						`drupal_uc_product_stock`
					where
						`drupal_uc_product_stock`.`nid` = {$node->nid}
					limit 1
				");
				foreach($base_products as $base_product) { //This foreach loop is just to get at the contents of the $base_product. There should only be one.
					echo("<input class=\"base_product_qty\" type=\"hidden\" value=\"{$base_product->qty}\" />");
					echo("<input class=\"base_product_track_stock\" type=\"hidden\" value=\"{$base_product->stock_is_being_tracked}\" />");
				}
				echo <<<HTML
<script type="text/javascript">
	function serialize_attributes() {
		//Create a JSON string out of the currently selected attributes. They must be in this array in numerical order to match what's
		//in `drupal_uc_product_adustments`.`combination`. There must be a way to have this happen automatically; for now I am just
		//switching elements 0 & 1 in the jQuery object because I swapped two attributes and it screwed things up.
		var aid;
		var \$attribute_select_boxes = jQuery('.attribute select');
		var attributes = {};
		\$attribute_select_boxes.each(function() {
			aid = jQuery(this).attr('name').substr(jQuery(this).attr('name').indexOf('[')+1, jQuery(this).attr('name').indexOf(']') - jQuery(this).attr('name').indexOf('[') - 1);
			attributes[aid] = jQuery(this).val();
		});
		var keys = [];
		for (var key in attributes) {
			keys.push(key);
		}
		keys.sort();
		var sorted_attributes = {};
		for (var i in keys) {
			sorted_attributes[keys[i]] = attributes[keys[i]];
		}
		var attribute_string = JSON.stringify(sorted_attributes);
		console.log(attribute_string);
		return attribute_string;
	}
	function update_qty_readout() {
		if (jQuery('.attribute').length) {
			//Use the JSON string created by serialize_attributes() to compare against the JSON
			//strings stored in the hidden fields. If a match is found, pull the quantity for that
			//field (located in the rel tag) and plug it into the readout.
			var selected_combination = serialize_attributes();
			var selected_qty = 'Please make a selection in all required fields.';
			jQuery('.attribute_combination').each(function() {
				if (jQuery(this).val()==selected_combination) {
					selected_qty = jQuery(this).attr('rel');
				}
			});
			jQuery('#qty_readout').html(selected_qty);
		} else {
			var selected_qty = jQuery('.base_product_qty').eq(0).val();
		}
		if (selected_qty==0) {
			jQuery('#fake_add_to_cart').show();
			jQuery('.node-add-to-cart').hide();
		} else {
			jQuery('#fake_add_to_cart').hide();
			jQuery('.node-add-to-cart').show();
		}
	}
	(function($) {
		$(function() {
			//Add a div where we can display the quantity readout.
			$('.attributes').after('<div class="field field-label-inline clearfix"><div class="field-label">Quantity Available As Configured:&nbsp;</div><div class="field-items"><div id="qty_readout" class="field-item even"></div></div></div>');
			//Add a fake "Add to cart" button that we can swap out if the quantity is zero.
			$('.node-add-to-cart').after('<input class="" id="fake_add_to_cart" type="button" value="Purchase" style="display: none;" />');
			$('#fake_add_to_cart').click(function() {
				alert('The product is not currently in stock as configured. However, it is available as a special order by calling +1 970 454 2642. There is a $60 surcharge for special order items.');
			});
			update_qty_readout();
			$('.attribute select').change(function() {
				update_qty_readout();
			});
		});
	} (jQuery));
</script>
HTML;
				if ($display_submitted) {
					echo("<div class=\"submitted\">$submitted</div>");
				}
				echo("<div class=\"content\" $content_attributes>");
					// We hide the comments and links now so that we can render them later.
					hide($content['comments']);
					hide($content['links']);
					echo(render($content));
				echo("</div>");
				echo(render($content['links']));
				echo(render($content['comments']));
				break;
			}
			default: {
				// echo("<h2><a $title_attributes href=\"$node_url\">$title</a></h2>".render($title_suffix));
				if ($display_submitted) {
					echo("<div class=\"submitted\">$submitted</div>");
				}
				echo("<div class=\"content\" $content_attributes>");
					// We hide the comments and links now so that we can render them later.
					hide($content['comments']);
					hide($content['links']);
					echo(render($content));
				echo("</div>");
				echo(render($content['links']));
				echo(render($content['comments']));
				break;
			}
		}
	}
echo("</div>");