<?php
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php
	if ($view->name == 'faqs') {
		echo("<ul class=\"faq\">");
		foreach($rows as $id => $row) {
			echo("<li><a href=\"#faq{$view->result[$id]->nid}\">{$view->style_plugin->row_plugin->nodes[$view->result[$id]->nid]->title}</a></li>");
		}
		echo("</ul>");
	}
		
	if (!empty($title)) {
		echo("<h3>$title</h3>");
	}
	foreach($rows as $id => $row) {
		switch($view->name) {
			case 'faqs': {
				$anchor = "<a name=\"faq{$view->result[$id]->nid}\"></a>";
				$output = "<div class=\"{$classes_array[$id]}\">$anchor $row<div class=\"clearfix\"></div></div>";
				break;
			}
			case 'hot_sale_clearance_items': {
				$output = "<div class=\"{$classes_array[$id]}\">$row<div class=\"clearfix\"></div></div>";
				break;
			}
			case 'slider_links': {
				$output = "<div class=\"slide {$classes_array[$id]}\">$row</div>";
				break;
			}
			default: {
				$output = "<div class=\"{$classes_array[$id]}\">$row</div>";
				break;
			}
		}
		echo($output);
	}
?>