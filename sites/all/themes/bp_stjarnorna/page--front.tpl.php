<!-- BP Stjärnorna-->
<div id="content-wrapper">
	<div id="header" class="container_12">
		<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"></a>
		<?php if ($main_menu || $secondary_menu): ?>
			<div id="navigation">
				<div class="section">
					<?php
						print theme(
							'links__system_secondary_menu',
							array(
								'links' => $secondary_menu,
								'attributes' => array(
									'id' => 'secondary-menu',
									'class' => array(
										'links',
										'inline',
										'clearfix',
										'grid_12'
									)
								),
								'heading' => array(
									'text' => t('Secondary menu'),
									'level' => 'h2',
									'class' => array(
										'element-invisible'
									)
								)
							)
						);
						echo("<div id=\"header_blocks\">");
							if ($page['header']) {
								echo(render($page['header']));
							}
						echo("</div>");
						print theme(
							'links__system_main_menu',
							array(
								'links' => $main_menu,
								'attributes' => array(
									'id' => 'main-menu',
									'class' => array(
										'links',
										'inline',
										'clearfix',
										'grid_12'
									)
								),
								'heading' => array(
									'text' => t('Main menu'),
									'level' => 'h2',
									'class' => array(
										'element-invisible'
									)
								)
							)
						);
					?>
				</div>
			</div> <!-- /.section, /#navigation -->
		<?php endif; ?>
	</div>
	<div id="main-wrapper" class="container_12">
		<div id="main" class="grid_12">
			<?php print $messages; ?>
			<?php if ($page['frontpage_slider']): ?>
				<div id="frontpage_slider" class="grid_12 alpha"><div class="section">
					<?php print render($page['frontpage_slider']); ?>
				</div></div>
				<div class="clearfix"></div>
			<?php endif; ?>
			<div id="triptych">
				<?php if ($page['frontpage_col_1']): ?>
					<div id="frontpage_col_1" class="grid_4 alpha"><div class="section">
						<?php print render($page['frontpage_col_1']); ?>
					</div></div>
				<?php endif; ?>
				<?php if ($page['frontpage_col_2']): ?>
					<div id="frontpage_col_2" class="grid_4"><div class="section">
						<?php print render($page['frontpage_col_2']); ?>
					</div></div>
				<?php endif; ?>
				<?php if ($page['frontpage_col_3']): ?>
					<div id="frontpage_col_3" class="grid_4 omega"><div class="section">
						<?php print render($page['frontpage_col_3']); ?>
					</div></div>
				<?php endif; ?>
				<div class="clearfix"></div>
			</div>
		</div> <!-- /#main, /#main-wrapper -->
	</div>
	<div class="push"></div>
</div>
<div id="footer"><div class="section">
	<?php print render($page['footer']); ?>
</div></div> <!-- /.section, /#footer -->